"""Administration for models. Allows editing of tables on admin site."""
from django.contrib import admin
from cia_portal.models import Account, Appliance, CIA, Category, Subcategory, \
        Component, Conclusion, Model, OperatingSystem, ProductFamily, \
        SupportEngineer, Version

class AccountAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']


class ApplianceAdmin(admin.ModelAdmin):
    list_display = ['asset_tag', 'account', 'version', 'os', 'install_date']
    list_filter = ('version', 'model', 'os', 'account')
    search_fields = ['asset_tag', 'hostname', 'install_date', 'ip_address',
            'mac', ]


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']


class SubcategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'category']
    list_filter = ('category',)
    search_fields = ['name']


class ComponentAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']


class ConclusionAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']


class CIAAdmin(admin.ModelAdmin):
    list_display = ['case_number', 'appliance', 'subject']
    list_filter = ('component', 'subcategory__category', 'subcategory')
    search_fields = ['case_number']


class ModelAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']


class OperatingSystemAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']


class ProductFamilyAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']


class SupportEngineerAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']


class VersionAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']


admin.site.register(Account, AccountAdmin)
admin.site.register(Appliance, ApplianceAdmin)
admin.site.register(CIA, CIAAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Subcategory, SubcategoryAdmin)
admin.site.register(Component, ComponentAdmin)
admin.site.register(Conclusion, ConclusionAdmin)
admin.site.register(Model, ModelAdmin)
admin.site.register(OperatingSystem, OperatingSystemAdmin)
admin.site.register(ProductFamily, ProductFamilyAdmin)
admin.site.register(SupportEngineer, SupportEngineerAdmin)
admin.site.register(Version, VersionAdmin)

# vim: tabstop=4 softtabstop=4 shiftwidth=4 expandtab foldmethod=indent
