"""
Data processing functions.
"""
import logging
from cia_portal.models import Account, Appliance, CIA, Category, Component, \
        Model, Subcategory, Version, ProductFamily, SupportEngineer
from datetime import datetime
from django.core.files.base import ContentFile
from django.db import IntegrityError
import csv
from cia_portal.csv_unicode import UnicodeReader
import re

LOGGER = logging.getLogger(__name__)

def fix_time(date_time):
    """Attempt to parse datetime string, return datetime object"""
    # Salesforce seems to use two different date formats, try each
    try:
        return datetime.strptime(date_time, "%m/%d/%Y %I:%M %p")
    except ValueError:
        pass

    try:
        return datetime.strptime(date_time, "%m/%d/%Y %H:%M")
    except ValueError:
        pass

    try:
        return datetime.strptime(date_time, "%Y-%m-%d")
    except ValueError:
        pass

    try:
        return datetime.strptime(date_time, "%Y/%m/%d")
    except ValueError:
        return None

def get_appliance(asset_name):
    """Given an asset name, return an Appliance"""
    # Example Input: Recovery-833 On- & Off-Premise Appliance [833-201-50061]
    # Example Input: Unitrends Enterprise Backup [25883d70-1988-4363-ad09-b85a7c82e68a]
    match = re.search(r'\[(.+)\]', asset_name)
    if match:
        asset_tag = match.group(1)

        if r'UEB' in asset_name or r'Unitrends Enterprise Backup' in asset_name:
            model = 'UEB'
        else:
            splits = asset_tag.split('-')
            model = '-'.join(splits[0:2])

        model_obj, _ = Model.objects.get_or_create(name=model)
        appliance_obj, _ = Appliance.objects.get_or_create(
                asset_tag=asset_tag, model=model_obj)

        return appliance_obj

    return None

def get_updated_appliance(appliance_str):
    """Return Model and Account objects based on the Appliance string"""
    # Example Input: 730-101-00047 [Unitrends, Inc]
    # Example Input: 020cf0dc-4842-486d-88ea-c1935646ec09 [ID Database Marketing]
    appliance_obj = None
    match = re.search(r'(\S+)\s*\[(.+)\]', appliance_str)

    if match:
        appliance_obj, _ = Appliance.objects.get_or_create(asset_tag=match.group(1))
        appliance_obj.account = Account.objects.get(name=match.group(2))

    return appliance_obj

def get_component(case_root_cause_detail, subject):
    """Return a Component from the Case Root Cause Detail and Subject"""
    if r'document' in case_root_cause_detail.lower():
        component_str = 'Documentation/Videos'
    elif r'database' in case_root_cause_detail.lower():
        component_str = 'Database'
    elif r'proactive hardware' in subject.lower():
        component_str = 'Hardware (Proactive)'
    elif r'no warranty issued' in case_root_cause_detail.lower():
        component_str = 'Hardware (No Warranty)'
    elif r'warranty issued' in case_root_cause_detail.lower():
        component_str = 'Hardware (Warranty)'
    else:
        # Salesforce report places additional data with component, remove it
        component_str = case_root_cause_detail.split('=')[0].strip()

    component_obj, _ = Component.objects.get_or_create(name=component_str)

    return component_obj

def get_categories(case_root_cause, case_root_cause_detail):
    """Return the Subcategory (and consequently, the Category) from the Case
    Root Cause and Case Root Cause Detail."""
    category_obj, _ = Category.objects.get_or_create(name=case_root_cause)

    if r'documentation' in case_root_cause.lower():
        subcategory_obj, _ = Subcategory.objects.get_or_create(
                name=case_root_cause_detail, category=category_obj)
    else:
        subcategory_obj, _ = Subcategory.objects.get_or_create(
                name='General', category=category_obj)

    return subcategory_obj

def get_warranty(case_root_cause_detail):
    """Determine whether a warranty was issued"""
    return r'warranty issued' in case_root_cause_detail.lower() and not \
        r'no warranty issued' in case_root_cause_detail.lower()

def import_csv(file_object):
    """Determine whether the file contains new or updated CIAs and dispatch to
       import_cias() or update_cias() respectively"""
    # Match the first row of the CSV against these to determine which function to call
    import_header = [u'Account Name', u'Case Number', u'Asset Name (Text)',
            u'Subject', u'Version', u'Module', u'Case Root Cause Detail',
            u'Bug #', u'Description', u'Why did the customer call for assistance',
            u'What was done to resolve the issue?', u'Group', u'Case Owner',
            u'Date/Time Opened', u'Open', u'Closed', u'Date/Time Closed',
            u'Case Root Cause', u'Case Record Type', u'Severity Level',
            u'Product Family', u'Asset Support Level']
    update_header = [u'Case Number', u'Appliance', u'Subject', u'Version', u'Category',
            u'Component', u'Product Family', u'Bug', u'Escalation', u'Knowledge Base',
            u'RCA', u'CLCA', u'Reimaged', u'Not Enough Information', u'Open Date',
            u'Close Date', u'Warranty Issued', u'Support Engineer', u'Severity',
            u'Test Case', u'Reason for Call', u'Action Taken']

    with ContentFile(file_object.read()) as file_handle:
        try:
            csv_reader = UnicodeReader(file_handle, quoting=csv.QUOTE_ALL)
            csv_header = csv_reader.next()
        except UnicodeDecodeError:
            LOGGER.error("UnicodeDecodeError")
        except:
            LOGGER.error("Could not parse file as CSV")

        if csv_header == import_header:
            LOGGER.info("Importing new CIAs")
            import_cias(csv_reader)
        elif csv_header == update_header:
            LOGGER.info("Updating existing CIAs")
            update_cias(csv_reader)
        else:
            LOGGER.warning("Unrecognized format")
            LOGGER.debug(csv_header)

        file_handle.close()

def import_cias(csv_reader):
    """Import data from Salesforce to the CIA portal"""
    for row in csv_reader:
        if not row or len(row) < 22 or not row[1]: # Ignore short/empty rows
            LOGGER.debug("Skipping short or empty row.")
            continue

        # Clean whitespace from ends of each column in row
        row = [column.strip() for column in row]
        # Create data container for current row, then modify data therein
        data = {
            'account': row[0],
            'case_number': row[1],
            'asset_name': row[2],
            'subject': row[3],
            'version': row[4],
            'case_root_cause_detail': row[6],
            'warranty': get_warranty(row[6]),
            'bug': row[7],
            'escalation': '',
            'description': row[8],
            'reason': row[9],
            'action': row[10],
            'support_engineer': row[12],
            'open_date': fix_time(row[13]),
            'close_date': fix_time(row[16]),
            'case_root_cause': row[17],
            'level': int(row[19].split()[1]) or 0,
            'product_family': row[20]
        }

        # Check bug for Escalation
        if data['bug']:
            match = re.search(r'esc\w*\s+(\d+)', data['bug'], re.IGNORECASE)
            if match:
                data['escalation'] = match.group(1)
                data['bug'] = ''

        # Set ForeignKey candidates to None (NULL) until found/created.
        account_obj = None
        if data['account']:
            account_obj, _ = Account.objects.get_or_create(name=data['account'])

        version_obj = None
        if data['version']:
            version_obj, _ = Version.objects.get_or_create(name=data['version'])

        product_family_obj = None
        if data['product_family']:
            product_family_obj, _ = ProductFamily.objects.get_or_create(
                    name=data['product_family'])

        support_engineer_obj = None
        support_engineer_obj, _ = SupportEngineer.objects.get_or_create(
                name=data['support_engineer'])

        subcategory_obj = None
        if data['case_root_cause'] and data['case_root_cause_detail']:
            subcategory_obj = get_categories(data['case_root_cause'],
                    data['case_root_cause_detail'])

        component_obj = get_component(data['case_root_cause_detail'],
                data['subject'])

        appliance_obj = get_appliance(data['asset_name'])

        if appliance_obj:
            appliance_obj.account = account_obj
            appliance_obj.version = version_obj
            appliance_obj.save()

        try:
            cia_obj, _ = CIA.objects.get_or_create(
                    action=data['action'],
                    appliance=appliance_obj,
                    bug=data['bug'],
                    escalation=data['escalation'],
                    case_number=data['case_number'],
                    close_date=data['close_date'],
                    component=component_obj,
                    original_component=component_obj,
                    description=data['description'],
                    open_date=data['open_date'],
                    product_family=product_family_obj,
                    support_engineer=support_engineer_obj,
                    reason=data['reason'],
                    subcategory=subcategory_obj,
                    severity=data['level'],
                    subject=data['subject'],
                    warranty_issued=data['warranty'])
            LOGGER.info(cia_obj)
        except IntegrityError as e:
            LOGGER.warning("Case " + data['case_number'] + ": " + str(e))

def update_cias(csv_reader):
    """Update the CIAs with new information"""
    for row in csv_reader:
        if not row or len(row) < 22 or not row[1]: # Ignore short/empty rows
            LOGGER.debug("Skipping short or empty row.")
            continue

        # Clean whitespace from ends of each column in row
        row = [column.strip() for column in row]

        # Create data container for current row, then modify data therein
        data = {
            'case_number': int(row[0]),
            'appliance': row[1],
            'subject': row[2],
            'version': row[3],
            'subcategory': row[4],
            'component': row[5],
            'product_family': row[6],
            'bug': row[7],
            'escalation': row[8],
            'knowledge_base': row[9] or None,
            'rca': row[10],
            'clca': row[11],
            'reimage': True if row[12] == u'True' else False,
            'not_enough_info': row[13],
            'open_date': fix_time(row[14]),
            'close_date': fix_time(row[15]),
            'warranty': True if row[16] == u'True' else False,
            'support_engineer': row[17],
            'severity': int(row[18]),
            'test_case': row[19],
            'reason': row[20],
            'action': row[21]
        }

        cia = CIA.objects.get(case_number=data['case_number'])

        # Update fields
        cia.subject = data['subject']
        cia.bug = data['bug']
        cia.escalation = data['escalation']
        cia.knowledge_base = data['knowledge_base']
        cia.reimage = data['reimage']
        cia.not_enough_info = data['not_enough_info']
        cia.warranty_issued = data['warranty']
        cia.severity = data['severity']
        cia.test_case = data['test_case']
        cia.reason = data['reason']
        cia.action = data['action']

        # Update fields which store ForeignKeys
        cia.appliance = get_updated_appliance(data['appliance'])
        cia.support_engineer, _ = SupportEngineer.objects.get_or_create(
                name=data['support_engineer'])
        cia.version, _ = Version.objects.get_or_create(name=data['version'])
        # subcategory
        # component
        # product_family

        cia.save()

# vim: tabstop=4 softtabstop=4 shiftwidth=4 expandtab foldmethod=indent
