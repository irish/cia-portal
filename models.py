"""
A model is the single, definitive source of data about your data. It contains
the essential fields and behaviors of the data being stored. Generally, each
model maps to a single database table.
"""
from django.db import models
from django.contrib.auth.models import User

class Account(models.Model):
    """Customer Account"""
    name = models.CharField(max_length=128, unique=True)

    def __unicode__(self):
        return "%s" % self.name

    class Meta(object):
        """Meta options"""
        ordering = ['name']


class Model(models.Model):
    """DPU Model/Type"""
    name = models.CharField(max_length=64, unique=True)

    class Meta(object):
        """Meta options"""
        ordering = ['name']

    def __unicode__(self):
        return "%s" % self.name


class OperatingSystem(models.Model):
    """Operating System"""
    name = models.CharField(max_length=64, unique=True)

    class Meta(object):
        """Meta options"""
        ordering = ['name']
        verbose_name = 'Operating System'

    def __unicode__(self):
        return "%s" % self.name


class ProductFamily(models.Model):
    """Product Family"""
    name = models.CharField(max_length=64, unique=True)

    class Meta(object):
        """Meta options"""
        ordering = ['name']
        verbose_name = 'Product Family'
        verbose_name_plural = 'Product Families'

    def __unicode__(self):
        return "%s" % self.name


class SupportEngineer(models.Model):
    """Support Engineer"""
    name = models.CharField(max_length=128, unique=True)

    class Meta(object):
        """Meta options"""
        ordering = ['name']
        verbose_name = 'Support Engineer'

    def __unicode__(self):
        return "%s" % self.name


class Version(models.Model):
    """Backup Professional Version"""
    name = models.CharField(max_length=64, unique=True)

    class Meta(object):
        """Meta options"""
        ordering = ['name']

    def __unicode__(self):
        return "%s" % self.name


class Category(models.Model):
    """CIA Major Category"""
    name = models.CharField(max_length=64)

    class Meta(object):
        """Meta options"""
        ordering = ['name']
        verbose_name_plural = 'Categories'

    def __unicode__(self):
        return "%s" % self.name


class Subcategory(models.Model):
    """CIA Subcategory"""
    name = models.CharField(max_length=128)
    category = models.ForeignKey(Category)

    def __unicode__(self):
        return "[%s] %s" % (self.category, self.name)

    class Meta(object):
        """Meta options"""
        ordering = ['category', 'name']
        unique_together = ('category', 'name')
        verbose_name_plural = 'Subcategories'


class Component(models.Model):
    """CIA Component"""
    name = models.CharField(max_length=64, unique=True)

    def __unicode__(self):
        return "%s" % self.name

    class Meta(object):
        """Meta options"""
        ordering = ['name']


class Conclusion(models.Model):
    """CIA Conclusion"""
    name = models.CharField(max_length=64)

    def __unicode__(self):
        return "%s" % self.name

    class Meta(object):
        """Meta options"""
        ordering = ['name']


class Appliance(models.Model):
    """Data schema for Unitrends Backup Appliances"""
    asset_tag = models.CharField('Asset Tag', max_length=64, unique=True)
    account = models.ForeignKey(Account, null=True, blank=True)
    model = models.ForeignKey(Model, null=True, blank=True)
    dpu_type = models.CharField('DPU Type', max_length=64, blank=True)
    version = models.ForeignKey(Version, null=True, blank=True)
    os = models.ForeignKey(OperatingSystem, verbose_name='Operating System',
            null=True, blank=True)
    mac = models.CharField('MAC Address', max_length=12, blank=True)
    hostname = models.CharField(max_length=64, blank=True)
    ip_address = models.GenericIPAddressField('IP Address', null=True, blank=True)
    install_date = models.DateField('Install Date', null=True, blank=True)

    def __unicode__(self):
        return "%s [%s]" % (self.asset_tag, self.account)

    class Meta(object):
        """Meta options"""
        get_latest_by = 'install_date'
        ordering = ['asset_tag']


class CIA(models.Model):
    """Customer Intervention Actions"""
    severity_choices = (
            (1, 'Level 1'),
            (2, 'Level 2'),
            (3, 'Level 3'),
            (4, 'Level 4'),
            (5, 'Level 5'),
    )

    appliance = models.ForeignKey(Appliance, null=True)
    case_number = models.PositiveIntegerField('Case Number', unique=True)
    subject = models.CharField(max_length=256)
    support_engineer = models.ForeignKey(SupportEngineer, null=True)
    severity = models.PositiveSmallIntegerField(choices=sorted(severity_choices))
    warranty_issued = models.BooleanField('Warranty Issued')
    open_date = models.DateField('Date Opened')
    close_date = models.DateField('Date Closed')
    subcategory = models.ForeignKey(Subcategory)
    component = models.ForeignKey(Component, related_name='Current Component')
    original_component = models.ForeignKey(Component,
            related_name='Original Component')
    product_family = models.ForeignKey(ProductFamily, verbose_name='Product Family',
            null=True, blank=True)
    reason = models.TextField(blank=True)
    action = models.TextField(blank=True)
    description = models.TextField()
    not_enough_info = models.TextField('Not Enough Information', blank=True)
    rca = models.TextField('RCA', blank=True)
    clca = models.TextField('CLCA', blank=True)
    reimage = models.BooleanField(default=False)
    bug = models.CharField(max_length=16, blank=True)
    escalation = models.CharField(max_length=16, blank=True)
    knowledge_base = models.PositiveIntegerField(null=True, blank=True)
    test_case = models.TextField(blank=True)
    conclusion = models.ForeignKey(Conclusion, null=True, blank=True)
    last_modified_by = models.ForeignKey(User, null=True, blank=True)

    def __unicode__(self):
        return "[%s] %s - %s" % (self.case_number, self.component, self.subject)

    @classmethod
    def header_row(cls):
        """Return list of fields to be used as a CSV header"""
        return ['Case Number',
                'Appliance',
                'Subject',
                'Version',
                'Category',
                'Component',
                'Product Family',
                'Bug',
                'Escalation',
                'Knowledge Base',
                'RCA',
                'CLCA',
                'Reimaged',
                'Not Enough Information',
                'Open Date',
                'Close Date',
                'Warranty Issued',
                'Support Engineer',
                'Severity',
                'Test Case',
                'Reason for Call',
                'Action Taken']

    @classmethod
    def data_row(cls, cia):
        """Return list of data to be used as a CSV row"""
        return [str(cia.case_number),
            cia.appliance.__unicode__() if cia.appliance else '',
            cia.subject or '',
            cia.appliance.version.__unicode__() if cia.appliance and \
                    cia.appliance.version else '',
            cia.subcategory.__unicode__() if cia.subcategory else '',
            cia.component.__unicode__() if cia.component else '',
            cia.product_family.__unicode__() if cia.product_family else '',
            cia.bug if cia.bug else '',
            cia.escalation if cia.escalation else '',
            str(cia.knowledge_base) if cia.knowledge_base else '',
            cia.rca or '',
            cia.clca or '',
            str(cia.reimage),
            str(cia.not_enough_info),
            cia.open_date.strftime("%Y-%m-%d"),
            cia.close_date.strftime("%Y-%m-%d"),
            str(cia.warranty_issued),
            cia.support_engineer.__unicode__(),
            str(cia.severity),
            cia.test_case or '',
            cia.reason or '',
            cia.action or '']

    class Meta(object):
        """Meta options"""
        get_latest_by = 'close_date'
        ordering = ['-close_date', 'clca', 'case_number']
        verbose_name = 'CIA'


# vim: tabstop=4 softtabstop=4 shiftwidth=4 expandtab foldmethod=indent
