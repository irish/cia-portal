#!/usr/bin/env python2.7
"""Provide UTF-8 support for CSV reading and writing."""

import csv, codecs, cStringIO

class UTF8Recoder(object):
	"""Iterator that reads an encoded stream and reencodes the input to UTF-8"""
	def __init__(self, f, encoding):
		self.reader = codecs.getreader(encoding)(f)

	def __iter__(self):
		return self

	def next(self):
		"""Return the next row, encoded in UTF-8"""
		return self.reader.next().encode("utf-8")

class UnicodeReader(object):
	"""A CSV reader which will iterate over lines in the CSV file "f", which is
	encoded in the given encoding"""

	def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
		f = UTF8Recoder(f, encoding)
		self.reader = csv.reader(f, dialect=dialect, **kwds)

	def next(self):
		"""Return the next row"""
		row = self.reader.next()
		return [unicode(s, "utf-8") for s in row]

	def __iter__(self):
		return self

class UnicodeWriter(object):
	"""A CSV writer which will write rows to CSV file "f", which is encoded in
	the given encoding"""

	def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
		# Redirect output to a queue
		self.queue = cStringIO.StringIO()
		self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
		self.stream = f
		self.encoder = codecs.getincrementalencoder(encoding)()

	def writerow(self, row):
		"""Write a single row"""
		self.writer.writerow([s.encode("utf-8") for s in row])
		# Fetch UTF-8 output from the queue...
		data = self.queue.getvalue()
		data = data.decode("utf-8")
		# ...and reencode it into the target encoding
		data = self.encoder.encode(data)
		# Write to the target stream
		self.stream.write(data)
		# Empty queue
		self.queue.truncate(0)

	def writerows(self, rows):
		"""Write all rows"""
		for row in rows:
			self.writerow(row)
