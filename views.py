"""Views for the CIA Portal. Prepare data for templates."""
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from cia_portal.data import import_csv
from cia_portal.forms import ApplianceForm, CIAForm, CIAImportForm
from cia_portal.models import Appliance, CIA, Category, Component, Subcategory
from datetime import date, datetime, timedelta
from re import match
import cia_portal.csv_unicode

def get_date_dict(request, latest=False):
    """Return a dictionary of useful dates in YYYY-MM-DD format"""
    date_dict = {
        'DEFAULT_START': "1970-01-01",
        'DEFAULT_END': date.strftime(date.today(), "%Y-%m-%d"),
        'START': request.GET.get('start_date', "1970-01-01"),
        'END': request.GET.get('end_date', date.strftime(date.today(), "%Y-%m-%d"))
    }

    date_dict['LAST_WEEK_START'], date_dict['LAST_WEEK_END'] = last_week_string()

    if latest:
        date_dict['START'], date_dict['END'] = date_dict['LAST_WEEK_START'], \
                date_dict['LAST_WEEK_END']

    return date_dict

def last_week():
    """Return datetime tuple of the last Friday to Thursday week"""
    start_date = date.today() - timedelta((date.today().weekday() - 4) % 7 + 7)
    end_date = start_date + timedelta(6)

    return start_date, end_date

def last_week_string():
    """Return string tuple of the last Friday to Thursday week"""
    start_date, end_date = last_week()
    start_date = date.strftime(start_date, "%Y-%m-%d")
    end_date = date.strftime(end_date, "%Y-%m-%d")

    return start_date, end_date

def cia_portal_redirect(request):
    """Redirect from / to /cia_portal/"""
    return HttpResponseRedirect('/cia_portal/')

def cia_index(request, component_id=None, complete=False, incomplete=False,
        latest=False, per_page=25, export=False, not_enough_info=0):
    """Paginated listing of CIAs"""
    # Filter by component
    if component_id:
        cias = CIA.objects.filter(component__id=component_id)
        component = get_object_or_404(Component, pk=component_id)
    else:
        cias = CIA.objects.all()
        component = None

    if not_enough_info == 1:
        cias = cias.exclude(not_enough_info=u'')
    elif not_enough_info == -1:
        cias = cias.exclude(not_enough_info__gt=u'')

    dates = get_date_dict(request, latest)

    cias = cias.filter(close_date__range=[dates['START'], dates['END']])

    # Filter by completeness
    if complete:
        cias = cias.exclude(clca=u'')
    elif incomplete:
        cias = cias.exclude(clca__gt=u'')

    # Export
    if export:
        return cia_export(cias)

    # Elements needed for navigation and filtering
    categories = Category.objects.order_by('name')
    components = Component.objects.order_by('name')
    paginator = Paginator(cias, per_page)
    page = request.GET.get('page')

    try:
        cias_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, return first page
        cias_page = paginator.page(1)
    except EmptyPage:
        # If page is out of range, return last page
        cias_page = paginator.page(paginator.num_pages)

    context = {'cias': cias_page, 'component': component, 'categories':
            categories, 'components': components, 'dates': dates}

    return render(request, 'cia_portal/cia_index.html', context)

def cia_index_query_set(request, cias, per_page=25):
    """Render a CIA QuerySet to a paginated CIA index"""
    # Elements needed for navigation and filtering
    categories = Category.objects.order_by('name')
    components = Component.objects.order_by('name')
    paginator = Paginator(cias, per_page)
    page = request.GET.get('page')

    dates = get_date_dict(request)

    try:
        cias_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, return first page
        cias_page = paginator.page(1)
    except EmptyPage:
        # If page is out of range, return last page
        cias_page = paginator.page(paginator.num_pages)

    context = {'cias': cias_page, 'categories': categories, 'components':
            components, 'dates': dates}

    return render(request, 'cia_portal/cia_index.html', context)

def cias_complete():
    """Return all complete CIAs"""
    cias = CIA.objects.exclude(clca=u'')
    return cias

def cias_incomplete():
    """Return all incomplete CIAs"""
    cias = CIA.objects.exclude(clca__gt=u'')
    return cias

def cia_detail(request, cia_id):
    """Detail view for given CIA"""
    cia = get_object_or_404(CIA, pk=cia_id)
    categories = Category.objects.order_by('name')
    components = Component.objects.order_by('name')
    dates = get_date_dict(request)

    # Because we switched from Bugzilla to JIRA, some of the older CIAs will
    # have Bugzilla issue numbers rather than JIRA issue keys. Make an attempt
    # to generate a URL to the correct issue tracking system.
    if cia.bug and match(r"\d+", cia.bug):
        cia.bug_url = "http://bugzilla.unitrends.com/show_bug.cgi?id="
    else:
        cia.bug_url = "https://unitrends.atlassian.net/browse/"

    if cia.escalation and match(r"\d+", cia.escalation):
        cia.esc_url = "http://escalations.unitrends.com/show_bug.cgi?id="
    else:
        cia.esc_url = "https://unitrends.atlassian.net/browse/"

    context = {'cia': cia, 'categories': categories, 'components': components,
            'dates': dates}

    return render(request, 'cia_portal/cia_detail.html', context)

def cia_detail_by_case(request, case_number):
    """Detail view for CIA by Salesforce case number"""
    cia = get_object_or_404(CIA, case_number=case_number)
    return HttpResponseRedirect('/cia_portal/' + str(cia.pk))

def cia_edit(request, cia_id):
    """Form for editing details for given CIA"""
    cia = get_object_or_404(CIA, pk=cia_id)
    categories = Category.objects.order_by('name')
    components = Component.objects.order_by('name')
    form = CIAForm(request.POST or None, instance=cia)

    if form.is_valid():
        form.save()
        return HttpResponseRedirect('..')

    context = {'cia': cia, 'categories': categories, 'components': components,
            'form': form}

    return render(request, 'cia_portal/cia_edit.html', context)

@login_required
def cia_import(request):
    """Form for importing CIA data"""
    if request.method == 'POST':
        form = CIAImportForm(request.POST, request.FILES)
        if form.is_valid():
            import_csv(request.FILES['csv_file'])
            return HttpResponseRedirect('..')
    else:
        form = CIAImportForm()
    categories = Category.objects.order_by('name')
    components = Component.objects.order_by('name')
    context = {'categories': categories, 'components': components,
            'form': form}

    return render(request, 'cia_portal/cia_import.html', context)

def cia_export_by_id(request, cia_id):
    """Export a single CIA by its ID"""
    cia = CIA.objects.filter(id=cia_id)
    return cia_export(cia)

def cia_export(cias):
    """Export QuerySet to .csv file"""
    time_stamp = datetime.now().strftime("%Y%m%d%H%M")
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = \
            'attachment; filename="CIAs-%s.csv"' % time_stamp

    writer = cia_portal.csv_unicode.UnicodeWriter(response)
    writer.writerow(CIA.header_row())

    for cia in cias:
        writer.writerow(CIA.data_row(cia))

    return response

def appliance_index(request):
    """Paginated list of all appliances"""
    appliances = Appliance.objects.order_by('asset_tag')
    paginator = Paginator(appliances, 25)
    page = request.GET.get('page')

    try:
        appliances_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, return first page
        appliances_page = paginator.page(1)
    except EmptyPage:
        # If page is out of range, return last page
        appliances_page = paginator.page(paginator.num_pages)

    categories = Category.objects.order_by('name')
    components = Component.objects.order_by('name')
    context = {'appliances': appliances_page, 'categories': categories,
            'components': components}
    return render(request, 'cia_portal/appliance_index.html', context)

def appliance_detail(request, appliance_id):
    """Detail view for given appliance"""
    appliance = get_object_or_404(Appliance, pk=appliance_id)
    categories = Category.objects.order_by('name')
    components = Component.objects.order_by('name')
    context = {'appliance': appliance, 'categories': categories,
            'components': components}
    return render(request, 'cia_portal/appliance_detail.html', context)

def appliance_edit(request, appliance_id):
    """Form for editing details for given appliance"""
    appliance = get_object_or_404(Appliance, pk=appliance_id)
    form = ApplianceForm(request.POST or None, instance=appliance)

    if form.is_valid():
        form.save()
        return HttpResponseRedirect('..')

    categories = Category.objects.order_by('name')
    components = Component.objects.order_by('name')
    context = {'appliance': appliance, 'categories': categories,
            'components': components, 'form': form}

    return render(request, 'cia_portal/appliance_edit.html', context)

def category_index(request, latest=False):
    """List of Categories, Subcategories, and number of associated CIAs"""
    # Elements needed for navigation and filtering
    categories = Category.objects.order_by('name')
    components = Component.objects.order_by('name')
    subcategories = Subcategory.objects.order_by('category')

    dates = get_date_dict(request, latest)

    sub_counts = [CIA.objects.filter(subcategory=subcategory,
        close_date__range=[dates['START'], dates['END']]).count()
        for subcategory in subcategories]

    subcategories_with_counts = zip(subcategories, sub_counts)

    context = {'categories': categories, 'components': components,
            'subcategories': subcategories, 'subcategories_with_counts':
            subcategories_with_counts, 'dates': dates}

    return render(request, 'cia_portal/category_index.html', context)

def cias_by_subcategory(request, subcategory_id, latest=False):
    """Render a QuerySet of CIAs of the given Subcategory"""
    dates = get_date_dict(request, latest)

    cias = CIA.objects.filter(subcategory__id=subcategory_id,
            close_date__range=[dates['START'], dates['END']])

    return cia_index_query_set(request, cias)

def cias_by_category(request, category_id, latest=False):
    """Render a QuerySet of CIAs of the given Category"""
    dates = get_date_dict(request, latest)

    cias = CIA.objects.filter(subcategory__category__id=category_id,
            close_date__range=[dates['START'], dates['END']])

    return cia_index_query_set(request, cias)

def component_index(request, latest=False, export=False):
    """List of all components"""
    components = Component.objects.order_by('name')

    dates = get_date_dict(request, latest)

    counts = [CIA.objects.filter(component_id=component,
        close_date__range=[dates['START'], dates['END']]).count()
        for component in components]

    categories = Category.objects.order_by('name')
    components_with_counts = zip(components, counts)

    if export:
        return component_index_export(components_with_counts)

    context = {'categories': categories, 'components': components,
            'components_with_counts': components_with_counts,
            'dates': dates}

    return render(request, 'cia_portal/component_index.html', context)

def component_index_export(components_with_counts):
    """Export component counts as .csv file"""
    time_stamp = datetime.now().strftime("%Y%m%d%H%M")
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = \
            'attachment; filename="components-%s.csv"' % time_stamp
    writer = cia_portal.csv_unicode.csv.writer(response)
    writer.writerow(['Component', 'Count'])

    for component, count in components_with_counts:
        writer.writerow([component, count])

    return response

def logout_view(request):
    """Logout of the current user"""
    logout(request)
    return HttpResponseRedirect('/cia_portal/')

def cia_search(request):
    """Search"""
    if 'query' in request.GET and request.GET['query']:
        query = request.GET['query']
    return cia_detail_by_case(request, query)

# vim: tabstop=4 softtabstop=4 shiftwidth=4 expandtab foldmethod=indent
