"""URL rewriting."""
from django.conf.urls import patterns, url

urlpatterns = patterns('cia_portal.views',
        # CIAs
        # Ex: /
        url(r'^$', 'cia_index'),
        # Ex: /cia_portal/5/
        url(r'^(?P<cia_id>\d+)/$', 'cia_detail'),
        # Ex: /cia_portal/5/edit/
        url(r'^(?P<cia_id>\d+)/edit/$', 'cia_edit'),
        # Ex: /cia_portal/5/export/
        url(r'^(?P<cia_id>\d+)/export/$', 'cia_export_by_id'),
        # Ex: /cia_portal/case/12345/
        url(r'^case/(?P<case_number>\d+)/$', 'cia_detail_by_case'),
        # Ex: /cia_portal/not_enough_info/
        url(r'^not_enough_info/$', 'cia_index', {'not_enough_info': 1}),
        # Ex: /cia_portal/not_enough_info/export/
        url(r'^not_enough_info/export/$', 'cia_index', {'not_enough_info': 1,
            'export': True}),
        # Ex: /cia_portal/import/
        url(r'^import/$', 'cia_import'),
        # Ex: /cia_portal/complete/
        url(r'^complete/$', 'cia_index', {'complete': True}),
        # Ex: /cia_portal/complete/export/
        url(r'^complete/export/$', 'cia_index', {'complete': True,
            'export': True}),
        # Ex: /cia_portal/incomplete/
        url(r'^incomplete/$', 'cia_index', {'incomplete': True}),
        # Ex: /cia_portal/incomplete/export/
        url(r'^incomplete/export/$', 'cia_index', {'incomplete': True,
            'export': True}),
        # Ex: /cia_portal/latest/
        url(r'^latest/$', 'cia_index', {'latest': True}),
        # Ex: /cia_portal/latest/complete/
        url(r'^latest/complete/$', 'cia_index', {'complete': True,
            'latest': True}),
        # Ex: /cia_portal/latest/incomplete/
        url(r'^latest/incomplete/$', 'cia_index', {'incomplete': True,
            'latest': True}),
        # Ex: /cia_portal/export/
        url(r'^export/$', 'cia_index', {'export': True}),
        # Ex: /cia_portal/latest/export/
        url(r'^latest/export/$', 'cia_index', {'latest': True, 'export': True}),
        # Ex: /cia_portal/?start_date=2014-05-01&end_date=2014-05-29
        # url(r'(?P<start_date>\d{4}-\d{2}-\d{2})&', '', {}),

        # Search
        # Ex: /cia_portal/search/query/
        url(r'^search/$', 'cia_search'),

        # Appliances
        # Ex: /cia_portal/appliance/
        url(r'^appliance/$', 'appliance_index'),
        # Ex: /cia_portal/appliance/5/
        url(r'^appliance/(?P<appliance_id>\d+)/$', 'appliance_detail'),
        # Ex: /cia_portal/appliance/5/edit/
        url(r'^appliance/(?P<appliance_id>\d+)/edit/$', 'appliance_edit'),

        # Components
        # Ex: /cia_portal/component/
        url(r'^component/$', 'component_index'),
        # Ex: /cia_portal/component/export/
        url(r'^component/export/$', 'component_index', {'export': True}),
        # Ex: /cia_portal/component/5/
        url(r'^component/(?P<component_id>\d+)/$', 'cia_index'),
        # Ex: /cia_portal/component/5/complete/
        url(r'^component/(?P<component_id>\d+)/complete/$',
            'cia_index', {'complete': True}),
        # Ex: /cia_portal/component/5/incomplete/
        url(r'^component/(?P<component_id>\d+)/incomplete/$',
            'cia_index', {'incomplete': True}),
        # Ex: /cia_portal/component/5/export/
        url(r'^component/(?P<component_id>\d+)/export/$', 'cia_index',
            {'export': True}),
        # Ex: /cia_portal/component/5/complete/export/
        url(r'^component/(?P<component_id>\d+)/complete/export/$', 'cia_index',
            {'complete': True, 'export': True}),
        # Ex: /cia_portal/component/5/incomplete/export/
        url(r'^component/(?P<component_id>\d+)/incomplete/export/$',
                'cia_index', {'incomplete': True, 'export': True}),

        # Categories
        # Ex: /cia_portal/category/
        url(r'^category/$', 'category_index'),
        # Ex: /cia_portal/category/export/
        url(r'^category/export/$', 'cias_by_category', {'export': True}),
        # Ex: /cia_portal/category/5/
        url(r'^category/(?P<category_id>\d+)/$', 'cias_by_category'),

        # Subcategories
        # Ex: /cia_portal/subcategory/
        url(r'^subcategory/$', 'category_index'),
        # Ex: /cia_portal/subcategory/export/
        url(r'^subcategory/export/$', 'cias_by_subcategory', {'export': True}),
        # Ex: /cia_portal/subcategory/5/
        url(r'^subcategory/(?P<subcategory_id>\d+)/$', 'cias_by_subcategory'),
)

urlpatterns += patterns('',
        # Users
        (r'^logout/$', 'django.contrib.auth.views.logout', {'next_page':
            '/cia_portal/'}),
        (r'^login/$', 'django.contrib.auth.views.login', {
            'template_name': 'cia_portal/login.html'}),
        )

# vim: tabstop=4 softtabstop=4 shiftwidth=4 expandtab foldmethod=indent
