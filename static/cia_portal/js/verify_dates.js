function verify_dates (date_to_check) {
	var regex = /\d{4}-\d{2}-\d{2}$/;
	var found = date_to_check.match(regex);

	if (found !== null) {
		str = found[0];
		year = parseInt(str.substring(0, 4));
		month = parseInt(str.substring(5, 7));
		day = parseInt(str.substring(8, 10));

		if (year < 0) { return false; }
		if (month < 1 || month > 12) { return false; }
		if (day < 0) { return false; }

		if (month == 4 || month == 6 || month == 9 || month == 11) {
			if (day > 30) { return false; }
		} else if (month == 2) {
			if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) {
				if (day > 29) { return false; }
			} else {
				if (day > 28) { return false; }
			}
		} else {
			if (day > 31) { return false; }
		}
		return true;
	}
	return false;
}

function latest_dates() {
	document.getElementById('start_date_field').value = document.getElementById('last_week_start').value;
	document.getElementById('end_date_field').value = document.getElementById('last_week_end').value;
}

function warn(start_date_to_check, end_date_to_check) {
	if (!(verify_dates(start_date_to_check) && verify_dates(end_date_to_check))) {
		var warning_div = document.getElementById("WARNING");
		if (warning_div) {
			warning_div.className = "error";
			warning_div.innerHTML = "<p>Invalid date, date range, or format. "
				+ "If your browser does not have a built-in date picker, "
				+ "please use the format YYYY-MM-DD.</p>";
			return false;
		}
	} else {
		var warning_div = document.getElementById("WARNING");
		if (warning_div) {
			warning_div.className = "";
			warning_div.innerHTML = "";
			return true;
		}
	}
}
