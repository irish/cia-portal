var width = 560;
var data = {{counts}};

var chart = d3.select("body").append("svg")
	.attr("class", "chart")
	.attr("width", width)
	.attr("height", 20 * data.length + 15)
	.append("g")
	.attr("transform", "translate(10, 15)");

var x = d3.scale.linear()
	.domain([0, d3.max(data)])
	.range([0, width - 15]);

var y = d3.scale.ordinal()
	.domain(data)
	.rangeBands([0, 20 * data.length]);

chart.selectAll("rect")
	.data(data)
	.enter().append("rect")
	.attr("y", function(d, i) {return i * 20;})
	.attr("width", x)
	.attr("height", 20);

chart.selectAll("text")
	.data(data)
	.enter().append("text")
	.attr("class", "bar")
	.attr("x", x)
	.attr("y", function(d, i) {return i * 20 + 10;})
	.attr("dx", -3) // padding-right
	.attr("dy", ".35em") //vertical-align: middle
	.attr("text-anchor", "end") // text-align: right
	.text(String);

chart.selectAll("line")
	.data(x.ticks(10))
	.enter().append("line")
	.attr("x1", x)
	.attr("x2", x)
	.attr("y1", 0)
	.attr("y2", 500)
	.style("stroke", "#CCCCCC");

chart.selectAll(".rule")
	.data(x.ticks(10))
	.enter().append("text")
	.attr("x", x)
	.attr("y", 0)
	.attr("dy", -3)
	.attr("text-anchor", "middle")
	.text(String);

chart.append("line")
	.attr("y1", 0)
	.attr("y2", 500)
	.style("stroke", "#000000");
