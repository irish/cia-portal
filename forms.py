"""Form related functions. Validation, processing, etc."""
from django import forms
from django.forms import Form, ModelForm
from django.core.files.uploadedfile import SimpleUploadedFile
from cia_portal.models import Appliance, CIA

class CIAImportForm(Form):
    """Form which imports CIA data (as CSV)"""
    csv_file = forms.FileField(label='Salesforce CSV')


class ApplianceForm(ModelForm):
    """Form generated from the Appliance Model"""
    class Meta(object):
        """Meta options"""
        model = Appliance


class CIAForm(ModelForm):
    """Form generated from the CIA Model"""
    class Meta(object):
        """Meta options"""
        model = CIA
        exclude = ('action', 'appliance', 'case_number', 'conclusion',
                'close_date', 'description', 'last_modified_by', 'open_date',
                'original_component', 'reason', 'severity', 'subject',
                'support_engineer', 'warranty_issued')

# vim: tabstop=4 softtabstop=4 shiftwidth=4 expandtab foldmethod=indent
