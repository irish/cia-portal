"""Unit tests which run when "manage.py test" is called. Test function names
must start with 'test_'."""

from django.test import TestCase
from datetime import datetime
from cia_portal import data
from cia_portal.models import Account, Appliance, Category, Component, Subcategory

class SimpleTest(TestCase):
    """Automated testing class"""
    def test_get_appliance_from_name(self):
        """Extract Appliance from string"""
        ueb = "Unitrends Enterprise Backup " \
                "[b88fa6d2-fbf3-4f86-aee6-3d660763d0e1]"
        ueb_result = data.get_appliance(ueb)
        self.assertEqual("b88fa6d2-fbf3-4f86-aee6-3d660763d0e1",
                ueb_result.asset_tag)
        self.assertEqual("UEB", ueb_result.model.name)
        dpu = "Recovery-833 On- & Off-Premise Appliance [833-201-50042]"
        dpu_result = data.get_appliance(dpu)
        self.assertEqual("833-201-50042", dpu_result.asset_tag)
        self.assertEqual("833-201", dpu_result.model.name)

    def test_get_component(self):
        """Determine component from Case Root Cause Detail and Subject"""
        # Test the various Documentation components
        component_obj, _ = Component.objects.get_or_create(name='Documentation/Videos')
        self.assertEqual(component_obj,
                data.get_component('Customer could not find issue/feature in documentation', 'Support Chat'))
        self.assertEqual(component_obj,
                data.get_component('Customer could not locate Self-Service Documentation/Videos on Website', 'Warranty Issued'))
        self.assertEqual(component_obj,
                data.get_component('Customer did not attempt to locate answer in Documentation/Videos', 'No Warranty Issued'))
        self.assertEqual(component_obj,
                data.get_component('Documentation incorrect', 'Proactive'))
        self.assertEqual(component_obj,
                data.get_component('Issue/Feature is not documented', 'Support Chat'))

        # Test the various Hardware components
        # Hardware (No Warranty)
        component_obj, _ = Component.objects.get_or_create(name='Hardware (No Warranty)')
        self.assertEqual(component_obj,
                data.get_component('No warranty issued=Filler text', 'Degraded RAID'))

        # Hardware (Warranty)
        component_obj, _ = Component.objects.get_or_create(name='Hardware (Warranty)')
        self.assertEqual(component_obj,
                data.get_component('Warranty issued=Filler text', 'Disk issues'))

        # Hardware (Proactive)
        component_obj, _ = Component.objects.get_or_create(name='Hardware (Proactive)')
        self.assertEqual(component_obj,
                data.get_component('Warranty issued=Filler text', 'Proactive Hardware'))
        self.assertEqual(component_obj,
                data.get_component('No warranty issued=Filler text', 'Proactive Hardware'))

    def test_get_warranty(self):
        """Determine whether a warranty was issued"""
        self.assertEqual(True, data.get_warranty('Warranty issued=Filler text'))
        self.assertEqual(False, data.get_warranty('No warranty issued=Filler text'))
        self.assertEqual(False, data.get_warranty('Anything else should be false'))

    def test_fix_time(self):
        """Interpret 12 and 24 hour datetime formats"""
        date_ref = datetime(2015, 3, 14, 21, 26)
        self.assertEqual(date_ref, data.fix_time('3/14/2015 9:26 PM'))
        self.assertEqual(date_ref, data.fix_time('3/14/2015 21:26'))
        self.assertEqual(None, data.fix_time('3/14/2015 25:70 AM'))
        self.assertEqual(None, data.fix_time('13/14/2015 21:26 AM'))

    def test_get_categories(self):
        """Categorize CIAs"""
        case_root_cause = 'Documentation/Videos'
        category_obj, _ = Category.objects.get_or_create(name=case_root_cause)
        case_root_cause_details = ['Customer could not find issue/feature in documentation',
                'Customer could not locate Self-Service Documentation/Videos on Website',
                'Customer did not attempt to locate answer in Documentation/Videos',
                'Documentation incorrect',
                'Issue/Feature is not documented']

        for case_root_cause_detail in case_root_cause_details:
            subcategory_obj, _ = Subcategory.objects.get_or_create(name=case_root_cause_detail, category=category_obj)
            self.assertEqual(subcategory_obj, data.get_categories(case_root_cause, case_root_cause_detail))

    def test_get_updated_appliance(self):
        """Get Appliance using format of previously rejected CIAs"""
        # 730-101-00047 [Unitrends, Inc]
        account_obj, _ = Account.objects.get_or_create(name='Unitrends, Inc')
        appliance_obj, _ = Appliance.objects.get_or_create(asset_tag='730-101-00047', account=account_obj)
        self.assertEqual(appliance_obj, data.get_updated_appliance('730-101-00047 [Unitrends, Inc]'))

# vim: tabstop=4 softtabstop=4 shiftwidth=4 expandtab foldmethod=indent
