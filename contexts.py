from django.core.urlresolvers import resolve

def appname(request):
    return {'appname': resolve(request.path).app_name}

# vim: tabstop=4 softtabstop=4 shiftwidth=4 expandtab foldmethod=indent
